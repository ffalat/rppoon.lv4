﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LV4
{
    class Adapter: IAnalytics
    {
        private Analyzer3rdParty analyticsService;
        public Adapter(Analyzer3rdParty service)
        {
            this.analyticsService = service;
        }
        private double[][] ConvertData(Dataset dataset)
        {
            //implementation missing!
            IList<List<double>> listofdata = dataset.GetData();
            int row = listofdata.Count;
            int column = listofdata[0].Count;
            double[][] datamatrix = new double[row][];
            for (int i = 0; i < row; i++)
            {
                datamatrix[i] = new double[column];
            }
            for (int i = 0; i < row; i++)
            {
                for (int j = 0; j < column; j++)
                {
                    datamatrix[i][j] = listofdata[i][j];

                }
            }
            return datamatrix;
        }
        public double[] CalculateAveragePerColumn(Dataset dataset)
        {
            double[][] data = this.ConvertData(dataset);
            return this.analyticsService.PerColumnAverage(data);
        }
        public double[] CalculateAveragePerRow(Dataset dataset)
        {
            double[][] data = this.ConvertData(dataset);
            return this.analyticsService.PerRowAverage(data);
        }
    }
}
