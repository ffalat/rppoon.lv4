﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LV4
{
    class Program
    {
        static void Main(string[] args)
        {
            Dataset data = new Dataset("file.txt");
            Analyzer3rdParty analyzer = new Analyzer3rdParty();
            Adapter adapter = new Adapter(analyzer);
            double[] result = adapter.CalculateAveragePerColumn(data);
            foreach (double r in result)
            {
                Console.WriteLine(r);
            }
            Console.ReadLine();
        }
    }
}
