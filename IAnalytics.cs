﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LV4
{
    interface IAnalytics
    {
        double[] CalculateAveragePerColumn(Dataset dataset);
        double[] CalculateAveragePerRow(Dataset dataset);
    }
}
